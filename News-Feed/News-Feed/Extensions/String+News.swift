//
//  String+News.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

extension String {
    
    func containsIgnoringCase(find: String) -> Bool {
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
}
