//
//  NewsListViewController.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsListViewController: UIViewController {
    
    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var underLineConstraint: NSLayoutConstraint!
    @IBOutlet weak var savedNewsButton: UIButton!
    @IBOutlet weak var newsButton: UIButton!
    var controller = NewsListController()
    var selectedTab: NewsFeedType = .loaded
    let savedNewsString = "SAVED NEWS"
    let newsString = "NEWS"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func animateTabBarFor(newsFeedType: NewsFeedType) {
        self.selectedTab = newsFeedType
        UIView.animate(withDuration: 0.3) {
            switch newsFeedType {
            case .loaded:
                self.savedNewsButton.setTitleColor(UIColor.lightGray, for: .normal)
                self.newsButton.setTitleColor(UIColor.darkGray, for: .normal)
                self.underLineConstraint.constant = 0
            case .saved:
                self.savedNewsButton.setTitleColor(UIColor.darkGray, for: .normal)
                self.newsButton.setTitleColor(UIColor.lightGray, for: .normal)
                self.underLineConstraint.constant = self.view.frame.width / 2
            }
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        searchChangeFor(newsFeedType: selectedTab, isSearching: true)
    }
    
    @IBAction func newsPressed(_ sender: UIButton) {
        animateTabBarFor(newsFeedType: .loaded)
        newsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0),
                                        at: .left,
                                        animated: true)
    }
    
    @IBAction func savePressed(_ sender: UIButton) {
        animateTabBarFor(newsFeedType: .saved)
        newsCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0),
                                        at: .right,
                                        animated: true)
    }
    
    func searchChangeFor(newsFeedType: NewsFeedType, isSearching: Bool) {
        switch newsFeedType {
        case .loaded:
            controller.onLoadedSearchPressed?(isSearching)
        case .saved:
            controller.onSaveSearchPressed?(isSearching)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let descriptionVC = segue.destination as? NewsDescriptionViewController
        descriptionVC?.controller.news = sender as? ArticleModel
        descriptionVC?.controller.newsFeedType = selectedTab
        descriptionVC?.onSave = {
            self.controller.getSavedArticles()
            self.newsCollectionView.reloadItems(at: [IndexPath(item: 1, section: 0)])
        }
    }
}


extension NewsListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection", for: indexPath) as! NewsCollectionCell
        cell.controller = controller
        cell.newsFeedType = indexPath.row == 0 ? .loaded : .saved
        cell.onNewsSelected = { news in
            self.performSegue(withIdentifier: "description", sender: news)
        }
        cell.onNewsDeleted = {
            self.controller.getSavedArticles()
            self.controller.searchedArticles = self.controller.savedArticles
            cell.newsTableView.reloadData()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as? NewsCollectionCell
        
        switch indexPath.row {
        case 0:
            animateTabBarFor(newsFeedType: .loaded)
            cell?.newsFeedType = .loaded
        case 1:
            animateTabBarFor(newsFeedType: .saved)
            cell?.newsFeedType = .saved
            controller.getSavedArticles()
        default:
            break
        }
        searchChangeFor(newsFeedType: selectedTab, isSearching: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as? NewsCollectionCell
        cell?.newsTableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height - 80)
    }
    
}
