//
//  NewsDescriptionViewController.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit

class NewsDescriptionViewController: UIViewController {
    
    @IBOutlet weak var newsDescriptionTableView: UITableView! {
        didSet {
            newsDescriptionTableView.rowHeight = UITableView.automaticDimension
            newsDescriptionTableView.estimatedRowHeight = 600
        }
    }
    var controller = NewsDescriptionController()
    var onSave: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if controller.newsFeedType == .saved {
            newsDescriptionTableView.tableFooterView = UIView()
        }
    }
    
    func configFavoriteButton(button: UIButton) {
        if !(controller.news?.favorite ?? false) {
            button.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        } else {
            button.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
        }
    }
    
    @IBAction func favoritePressed(_ sender: UIButton) {
        guard controller.newsFeedType != .saved else { return }
        if let isfavorite = controller.news?.favorite {
            controller.news?.favorite = !isfavorite
        } else {
            controller.news?.favorite = true
        }
        configFavoriteButton(button: sender)
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func savePressed(_ sender: UIButton) {
        controller.saveAtricle()
        onSave?()
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NewsDescriptionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsBigImageCell", for: indexPath) as! NewsBigImageCell
            cell.imageUrlString = controller.news?.urlToImage
            configFavoriteButton(button: cell.favoriteButton)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDescriptionCell", for: indexPath) as! NewsDescriptionCell
            cell.decriptionLabel.text = controller.news?.content
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

