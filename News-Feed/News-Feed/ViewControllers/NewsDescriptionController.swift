//
//  NewsDescriptionController.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class NewsDescriptionController: NSObject {
    
    var newsFeedType: NewsFeedType?
    var news: ArticleModel?
    var realm: Realm?
    
    func saveAtricle() {
        do {
            realm = try Realm()
            if let user = realm?.objects(User.self).first {
                if user.aritcles?.filter({$0.url == news?.url}).count == 0, let news = news {
                    try? realm?.write {
                        if user.aritcles?.count == 0 {
                            user.aritcles = [news]
                        } else {
                            user.aritcles?.append(news)
                        }
                        realm?.add(user, update: true)
                    }
                }
            } else if let news = news {
                try? realm?.write {
                    let user = User()
                    user.aritcles = [news]
                    realm?.add(user)
                }
            }
        } catch {
            print("no realm!!!")
        }
    }
    
}
