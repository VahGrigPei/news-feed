//
//  NewsCell.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    var onDelete:((ArticleModel) -> Void)?
    var imageUrl: URL?
    var newsFeedType: NewsFeedType? {
        didSet {
            closeButton.isHidden = newsFeedType == .loaded
        }
    }
    var news: ArticleModel! {
        didSet {
            headerLabel.text = news.author
            descriptionLabel.text = news.description
            if let urlSrting = news.urlToImage {
                imageUrl = URL(string: urlSrting)
            }
            if let imageUrl = imageUrl {
                
                let filter = AspectScaledToFillSizeFilter(size: CGSize(width: 76, height: 76))
                newsImageView?.af_setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "noImage"), filter: filter)
            }
        }
    }
    
    @IBAction func closedPressed(_ sender: UIButton) {
        onDelete?(news)
    }
    
}
