//
//  NewsCollectionCell.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit

enum NewsFeedType {
    case loaded
    case saved
}

class NewsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsTableView: UITableView! {
        didSet {
            newsTableView.rowHeight = UITableView.automaticDimension
            newsTableView.keyboardDismissMode = .interactive
            newsTableView.keyboardDismissMode = .onDrag
            newsTableView.estimatedRowHeight = 100
            newsTableView.tableFooterView = UIView()
        }
    }
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var retryButton: UIButton!
    
    var controller: NewsListController?
    var onNewsSelected: ((ArticleModel?) -> Void)?
    var onNewsDeleted: (() -> Void)?
    var newsFeedType: NewsFeedType? {
        didSet {
            loadFirstPage()
        }
    }
    var searchingText = ""
    var isSearching = false
    var isFetching = false
    var page = 1
    
    func loadFirstPage() {
        
        switch newsFeedType {
        case .saved?:
            controller?.getSavedArticles()
            controller?.searchedArticles = controller?.savedArticles ?? [ArticleModel]()
            newsTableView.reloadData()
            activityView.isHidden = true
            controller?.onSaveSearchPressed = { isSearching in
                self.animateFor(isSearching: isSearching)
            }
        case .loaded?:
            guard controller?.newsArray.count == 0, !isFetching else {
                controller?.searchedArticles = controller?.newsArray ?? [ArticleModel]()
                newsTableView.reloadData()
                return
            }
            activityView.startAnimating()
            isFetching = true
            loadArticles()
            controller?.onLoadedSearchPressed = { isSearching in
                self.animateFor(isSearching: isSearching)
            }
        default: break
        }
        
    }
    
    func loadArticles() {
        controller?.newsFeedfor(page: page) { (newsModel, error) in
            self.isFetching = false
            self.activityView.stopAnimating()
            self.activityView.isHidden = true
            if newsModel != nil {
                self.newsTableView.reloadData()
                self.controller?.totalResults = newsModel?.totalResults ?? 0
                self.page += 1
            } else if newsModel == nil, self.page == 0 {
                self.retryButton.isHidden = false
            }
        }
    }
    
    func loadSearchedArticles() {
        controller?.getNewsFor(searchText: searchingText, page: page, completion: { (newsModel, error) in
            self.isFetching = false
            if newsModel != nil {
                self.newsTableView.reloadData()
                self.page += 1
            }
        })
    }
    
    func animateFor(isSearching: Bool) {
        UIView.animate(withDuration: 0.3) {
            if isSearching {
                self.tableViewTopConstraint.constant = 71
            } else {
                self.tableViewTopConstraint.constant = 15
            }
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func retryPressed(_ sender: Any) {
        loadFirstPage()
    }
    
}

extension NewsCollectionCell: UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller?.searchedArticles.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.news = controller?.searchedArticles[indexPath.row]
        cell.newsFeedType = newsFeedType
        cell.onDelete = { article in
            self.controller?.removeArticle(article: article)
            self.onNewsDeleted?()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        onNewsSelected?(controller?.searchedArticles[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        let upcomingRows = indexPaths.map { $0.row }
        if let maxIndex = upcomingRows.max() {
            let newsCount = (controller?.newsArray.count ?? 0)
            if maxIndex > (newsCount - 2) &&
                !isFetching &&
                (newsCount < (controller?.totalResults ?? 0)) {
                
                if newsFeedType == .loaded {
                    self.isFetching = true
                    if isSearching {
                        loadSearchedArticles()
                    } else {
                        loadArticles()
                    }
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return newsFeedType == .saved
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete),
            let article = controller?.savedArticles[indexPath.row] {
            controller?.removeArticle(article: article)
            onNewsDeleted?()
        }
    }
    
}

extension NewsCollectionCell: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchingText = searchText
        guard let controller = controller else { return }
        if searchText.isEmpty, let newsType = newsFeedType {
            switch newsType {
            case .loaded:
                controller.searchedArticles = controller.newsArray
            case .saved:
                controller.searchedArticles = controller.savedArticles
            }
        } else if let newsType = newsFeedType {
            switch newsType {  // searching in saved articles
            case .saved:
                let prefixArray = controller.savedArticles.filter {($0.author?.hasPrefix(searchText) ?? false)}
                let containArray = controller.savedArticles.filter {(($0.author?.containsIgnoringCase(find: searchText) ?? false) && !prefixArray.contains(where: { $0.author == $0.author}))}
                controller.searchedArticles = prefixArray + containArray
            default: break
            }
        }
        newsTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchingText = searchBar.text ?? ""
        switch self.newsFeedType {
        case .loaded?:
            isFetching = true
            self.controller?.searchedArticles.removeAll()
            loadSearchedArticles()
        default: break
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.endEditing(true)
        self.animateFor(isSearching: false)
        loadFirstPage()
    }
    
}

