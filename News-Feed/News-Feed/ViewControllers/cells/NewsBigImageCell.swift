//
//  NewsBigImageCell.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit

class NewsBigImageCell: UITableViewCell {
    
    @IBOutlet weak var bigImageView: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var imageUrlString: String? {
        didSet {
            if let urlString = imageUrlString,
                let url = URL(string: urlString) {
                bigImageView.af_setImage(withURL: url)
            }
        }
    }
    
}
