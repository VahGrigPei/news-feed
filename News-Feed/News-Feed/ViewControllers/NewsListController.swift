//
//  NewsListController.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class NewsListController: NSObject {
    
    var newsArray = [ArticleModel]()
    var savedArticles = [ArticleModel]()
    var searchedArticles = [ArticleModel]()
    var newsCategory: String?
    var realm: Realm?
    var totalResults = 0
    let pageSize = 20
    let country = "us"
    
    var onSaveSearchPressed: ((Bool) -> Void)?
    var onLoadedSearchPressed: ((Bool) -> Void)?
    
    func getSavedArticles() {
        do {
            realm = try Realm()
            if let user = realm?.objects(User.self).first, let articles = user.aritcles {
                self.savedArticles = articles
            }
        } catch {
            print("no realm!!!")
        }
    }
    
    func removeArticle(article: ArticleModel) {
        do {
            realm = try Realm()
            if let user = realm?.objects(User.self).first {
                if user.aritcles?.filter({$0.url == article.url}).count != 0 {
                    try? realm?.write {
                        user.aritcles?.removeAll{$0.url == article.url}
                        realm?.add(user, update: true)
                    }
                }
            }
        } catch {
            print("no realm!!!")
        }
    }
    
    func newsFeedfor(page: Int, completion: @escaping ((NewsModel?, Error?) -> Void) ) {
        APIClient.shared.requestNewsFor(country: "us", category: newsCategory, page: page, size: pageSize) { (newsModel, error) in
            if error == nil, let articles = newsModel?.articles {
                self.newsArray = self.newsArray + articles
                self.searchedArticles = self.newsArray
            }
            completion(newsModel, error)
        }
    }
    
    func getNewsFor(searchText: String, page: Int, completion: @escaping ((NewsModel?, Error?) -> Void)) {
        APIClient.shared.requestNewsForSearch(searachText: searchText, page: page, size: pageSize) { (newsModel, error) in
            if error == nil, let articles = newsModel?.articles {
                self.searchedArticles = self.searchedArticles + articles
            }
            completion(newsModel, error)
        }
    }
    
}
