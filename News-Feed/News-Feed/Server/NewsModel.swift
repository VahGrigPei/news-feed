//
//  NewsModel.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct NewsModel: Codable {
    let status: String?
    let totalResults: Int?
    let articles: [ArticleModel]?
}

