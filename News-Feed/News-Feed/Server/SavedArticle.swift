//
//  SavedArticle.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class SavedArticle: Object {
    
    var author: String?
    var title: String?
    var descriptionText: String?
    var url: String?
    var urlToImage: String?
    var imageData: Data?
    var content: String?
    var favorite: Bool?
    
}
