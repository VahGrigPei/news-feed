//
//  User.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    @objc dynamic var id = 0
    @objc private dynamic var structData: Data? = nil

    var aritcles: [ArticleModel]? {
        get {
            if let data = structData {
                return try? JSONDecoder().decode([ArticleModel].self, from: data)
            }
            return nil
        }
        set {
            structData = try? JSONEncoder().encode(newValue)
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
