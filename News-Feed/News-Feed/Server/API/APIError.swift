//
//  File.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

enum APIError: Int, LocalizedError {
    
    case unknownError
    
    public var errorDescription: String? {
        switch self {
        case .unknownError: return "Unknown server error"
        }
    }
}
