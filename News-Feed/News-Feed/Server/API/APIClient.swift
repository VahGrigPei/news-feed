//
//  APIClient.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import Alamofire

class APIClient: RequestAdapter {
    
    private let apiUrl: URL
    let manager: SessionManager
    let baseUrlString: String
    
    
    
    static let shared: APIClient = APIClient(configuration: .default,
                                             baseUrlString: "https://newsapi.org/v2/top-headlines")
    
    init(configuration: URLSessionConfiguration, baseUrlString: String) {
        self.baseUrlString = baseUrlString
        self.apiUrl = URL(string: baseUrlString)!
        self.manager = SessionManager(configuration: configuration)
        self.manager.adapter = self
    }
    
    func apiUrl(for path: String) -> URL {
        return URL(string:"\(baseUrlString + path)")!
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var request = urlRequest
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer 9d0500a4463646c3b18d05b6cf060afd", forHTTPHeaderField: "Authorization")
        return request
    }
    
    
}


// news call endpoint

extension APIClient {
    
    @discardableResult func requestNewsFor(country: String, category: String?, page: Int, size: Int, completion: @escaping (NewsModel?, Error?) -> Void) -> DataRequest {
        
        var urlString = "?country=\(country)&page=\(page)&pageSize=\(size)"
        if let category = category {
            urlString += "&category=\(category)"
        }
        
        let url = apiUrl(for: urlString)
        return manager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: JSONEncoding.default)
            .validateForNewsFeedApiError()
            .responseJSONDecodable { (response: DataResponse<NewsModel>) in
                completion(response.value, response.error)
        }
    }
    
    @discardableResult func requestNewsForSearch(searachText: String, page: Int, size: Int, completion: @escaping (NewsModel?, Error?) -> Void) -> DataRequest {
        let secureText = searachText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let url = apiUrl(for: "?&q=\(secureText)&page=\(page)&pageSize=\(size)")
        return manager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: JSONEncoding.default)
            .validateForNewsFeedApiError()
            .responseJSONDecodable { (response: DataResponse<NewsModel>) in
                completion(response.value, response.error)
        }
    }
    
}

