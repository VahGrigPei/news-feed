//
//  File.swift
//  News-Feed
//
//  Created by Vahan Grigoryan on 4/6/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct NewsFeedError: LocalizedError, Codable {
    
    let error: String?
    
    var errorDescription: String? { return error }
    var localizedDescription: String? { return error }
    
}
